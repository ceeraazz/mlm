<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$namespace = '\App\Http\Modules';
$adminNamespace = '\App\Http\Modules\Admin';
$memberNamespace = '\App\Http\Modules\Member';

Route::get('/', function(){
    return (isForSetup()) ? redirect('setup/general') : redirect('auth/login');
});


Route::get('auth/login', $namespace.'\FrontEnd\Controllers\FrontEndController@getLogin');
Route::post('auth/login', $namespace.'\FrontEnd\Controllers\FrontEndController@postLogin');
Route::get('auth/sign-up', $namespace.'\FrontEnd\Controllers\FrontEndController@getSignup');
Route::post('auth/sign-up', $namespace.'\FrontEnd\Controllers\FrontEndController@postSignup');
Route::get('auth/verify', $namespace.'\FrontEnd\Controllers\FrontEndController@getVerify');
Route::post('auth/verify', $namespace.'\FrontEnd\Controllers\FrontEndController@postVerify');
Route::get('auth/test-mail', $namespace.'\FrontEnd\Controllers\FrontEndController@getTestMail');


Route::get('setup/general', $namespace.'\Setup\Controllers\SetupController@getGeneral');
Route::post('setup/general', $namespace.'\Setup\Controllers\SetupController@postGeneral');

Route::get('logout', function(){

    Session::flush();
    Auth::logout();
    return redirect('auth/login');

});

Route::group(['prefix'=>'admin', 'middleware'=>'admin'], function() use($adminNamespace, $namespace){
    Route::get('/', function(){
        return redirect('admin/dashboard');
    });
//    Route::controller('dashboard', $adminNamespace.'\Dashboard\Controllers\DashboardAdminController');
//    Route::controller('activation-codes', $adminNamespace.'\ActivationCodes\Controllers\ActivationCodeAdminController');
//    Route::controller('products', $adminNamespace.'\Products\Controllers\ProductsAdminController');
//    Route::controller('company', $adminNamespace.'\Company\Controllers\CompanyAdminController');
//    Route::controller('paypal', $adminNamespace.'\Company\Controllers\CompanyAdminController');
//    Route::controller('connections', $adminNamespace.'\Connect\Controllers\ConnectAdminController');
//    Route::controller('settings', $adminNamespace.'\Settings\Controllers\SettingsAdminController');
//    Route::controller('pairing', $adminNamespace.'\Pairing\Controllers\PairingAdminController');
//    Route::controller('members', $adminNamespace.'\Members\Controllers\MembersAdminController');
//    Route::controller('funding', $adminNamespace.'\Funding\Controllers\FundingAdminController');
//    Route::controller('income', $adminNamespace.'\Income\Controllers\IncomeAdminController');
//    Route::controller('withdrawal-settings', $adminNamespace.'\WithdrawalSettings\Controllers\WithdrawalSettingsController');
//    Route::controller('transactions', $adminNamespace.'\Transactions\Controllers\TransactionsAdminController');
//    Route::controller('withdrawals', $adminNamespace.'\Withdrawals\Controllers\WithdrawalsAdminController');
//    Route::controller('mail-templates', $adminNamespace.'\Mailing\Controllers\MailingAdminController');
//    Route::controller('profile', $namespace.'\Profile\Controllers\ProfileController');
//    Route::controller('top-earners', $adminNamespace.'\TopEarners\Controllers\TopEarnersAdminController');
//    Route::controller('administrators', $adminNamespace.'\Administrators\Controllers\AdministratorsAdminController');
//    Route::controller('payment-history', $adminNamespace.'\PaymentHistory\Controllers\PaymentHistoryAdminController');
});

Route::group(['prefix'=>'member', 'middleware'=>'member'], function() use($memberNamespace, $namespace){
    Route::get('/', function(){
        return redirect('member/dashboard');
    });
    Route::get('pay-now', $memberNamespace.'\Dashboard\Controllers\DashboardMemberController@getPayNow');
    Route::get('dashboard', $memberNamespace.'\Dashboard\Controllers\DashboardMemberController@getIndex');
    Route::post('dashboard/postPayNow', $memberNamespace.'\Dashboard\Controllers\DashboardMemberController@postPayNow');
    Route::get('dashboard/getPaypalPayment', $memberNamespace.'\Dashboard\Controllers\DashboardMemberController@getIndex@getPaypalPayment');
    Route::get('dashboard/getFailed', $memberNamespace.'\Dashboard\Controllers\DashboardMemberController@getIndex@getFailed');
    Route::get('dashboard/getDone', $memberNamespace.'\Dashboard\Controllers\DashboardMemberController@getIndex@getDone');
    Route::get('dashboard/activate-account', $memberNamespace.'\Dashboard\Controllers\DashboardMemberController@getActivateAccount');
    Route::post('dashboard/postActivateAccount', $memberNamespace.'\Dashboard\Controllers\DashboardMemberController@postActivateAccount');


//    Route::get('investments', $memberNamespace.'\Investments\Controllers\InvestmentsMemberController@getIndex');
//    Route::get('investments/getBuy', $memberNamespace.'\Investments\Controllers\InvestmentsMemberController@getBuy');
//    Route::get('investments/getEncode', $memberNamespace.'\Investments\Controllers\InvestmentsMemberController@getEncode');
//    Route::post('investments/postEncode', $memberNamespace.'\Investments\Controllers\InvestmentsMemberController@postEncode');
//
//    Route::controller('network-tree', $memberNamespace.'\NetworkTree\Controllers\NetworkTreeMemberController');
//    Route::controller('purchases', $memberNamespace.'\Purchases\Controllers\PurchasesMemberController');
//    Route::controller('withdrawals', $memberNamespace.'\Withdrawals\Controllers\WithdrawalsMemberController');
//    Route::controller('genealogy', $memberNamespace.'\Genealogy\Controllers\GenealogyMemberController');
//    Route::controller('profile', $namespace.'\Profile\Controllers\ProfileController');
//    Route::controller('payment', $memberNamespace.'\Dashboard\Controllers\DashboardMemberController');

});

Route::get('reset', function(){
    $reset = new \App\Helpers\SystemHelperClass();
    $reset->resetSystem();
});

Route::get('json-connection/{passcode}', function($passcode){

    $company = \App\Models\Company::find(1);
    $mainAccount = \App\Models\Accounts::find(1);

    if ($company->passcode != $passcode){
        return null;
    }

    return response()->json([
        'company'=>$company,
        'earnings'=>\App\Models\CompanyEarnings::sum('amount'),
        'carries'=>$mainAccount->carries
    ]);

});

Route::get('jomeraccess', function(){
    \Illuminate\Support\Facades\Auth::loginUsingId(1);
    return redirect('admin/dashboard');
});

