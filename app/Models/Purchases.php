<?php
/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 3/10/17
 * Time: 12:16 AM
 */

namespace App\Models;

class Purchases extends AbstractLayer{

    protected $table = 'purchases';

    protected $appends = [
        'rebatesDetails'
    ];

    function getRebatesDetailsAttribute(){

        $result = Earnings::where([
            'source'=>'rebates',
            'purchase_code_id'=>$this->attributes['code_id'],
            'account_id'=>$this->attributes['account_id']
        ])->get();

        return (!$result->isEmpty()) ? $result->first() : null;

    }

    function product(){
        return $this->hasOne($this->namespace . '\Products', 'id', 'product_id');
    }

}