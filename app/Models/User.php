<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

class User extends AbstractLayer implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword, Billable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $validEarningSources = [
        REBATES_EARNINGS,
        UNILEVEL_EARNINGS,
        DIRECT_REFERRAL_EARNINGS,
        PAIRING_EARNINGS,
        SYSTEM_GENERATED
    ];

    protected $appends = [
        'earnings',
        'account',
        'withdrawn',
        'unilevelIncome',
        'rebatesIncome',
        'pairingIncome',
        'referralIncome',
        'remainingBalance',
        'purchasedCodes',
        'purchasedCodesTotal',
        'directReferral',
        'gcIncome',
        'purchasedProductCodes',
        'purchasedProductCodesTotal',
        'overallExpenses',
        'eWallet',
        'downlines',
        'downlineCount',
        'modulesArray'
    ];

    function details(){
        return $this->hasOne($this->namespace.'\Details', 'id', 'user_details_id');
    }

    function authDetails(){
        return $this->hasOne($this->namespace.'\User', 'user_Details_id', 'id');
    }

    function getEarningsAttribute(){

        return Earnings::where('user_id', $this->attributes['id'])->sum('amount');

    }

    function accounts(){
        return $this->hasMany($this->namespace.'\Accounts', 'user_id', 'id');
    }

    function getAccountAttribute(){

        $id = '';

        $account = $this->accounts()->first();

        return $account;

    }

    function getWithdrawnAttribute(){
        return Withdrawals::where('user_id', $this->attributes['id'])->whereIn('status', ['pending', 'approved'])->sum('amount');
    }

    function getUnilevelIncomeAttribute(){
        return Earnings::where([
            'user_id'=>$this->attributes['id'],
            'source'=>UNILEVEL_EARNINGS
        ])->sum('amount');
    }

    function getRebatesIncomeAttribute(){
        return Earnings::where([
            'user_id'=>$this->attributes['id'],
            'source'=>REBATES_EARNINGS
        ])->sum('amount');
    }

    function getPairingIncomeAttribute(){
        return Earnings::where([
            'user_id'=>$this->attributes['id'],
            'source'=>PAIRING_EARNINGS
        ])->sum('amount');
    }

    function getReferralIncomeAttribute(){
        return Earnings::where([
            'user_id'=>$this->attributes['id'],
            'source'=>DIRECT_REFERRAL_EARNINGS
        ])->sum('amount');
    }

    function getGcIncomeAttribute(){
        return Earnings::where([
            'user_id'=>$this->attributes['id'],
            'source'=>GC_EARNINGS
        ])->sum('amount');
    }

    function getRemainingBalanceAttribute(){

        $earnings = Earnings::where('user_id', $this->attributes['id'])
            ->whereIn('source', $this->validEarningSources)
            ->get();
        $charge = config('system.repeat_sales_charge');
        $balance = 0;

        if (isset($this->account->code->type) and $this->account->code->type == FREE_CODE){
            $company = getCompanyObject();
            $balance = ($company->entry_fee * (-1));
        }

        foreach ($earnings as $earningRow){
            $totalCharge = ($charge > 0) ? ($charge/100)*$earningRow->amount : 0;
            $balance += ($earningRow->amount - $totalCharge);
        }

        return $balance - ($this->overallExpenses);
//        return $this->earnings - $this->overallExpenses;

    }

    function getPurchasedCodesAttribute(){
        return ActivationCodes::where('user_id', $this->attributes['id']);
    }

    function getPurchasedCodesTotalAttribute(){
        $entry = codePurchaseAmount();
        $codes = $this->purchasedCodes->where([
            'paid_by_balance'=>'true'
        ]);
        return $entry * $codes->count();
    }

    function getDirectReferralAttribute(){
        $accountID = @$this->account->id;
        return Accounts::where('sponsor_id', $accountID);
    }

    function getPurchasedProductCodesAttribute(){
        return PurchaseCodes::where('owner_id', $this->attributes['id']);
    }

    function getPurchasedProductCodesTotalAttribute(){
        $total = 0;
        $codes = $this->purchasedProductCodes->get();
        foreach ($codes as $code){
            $total+= $code->purchase_value;
        }

        return $total;
    }

    function getOverallExpensesAttribute(){
        return ($this->withdrawn + $this->purchasedCodesTotal) + $this->purchasedProductCodesTotal;
    }

    function getEWalletAttribute(){
        return EWallet::where('account_id', $this->account->id)->sum('amount');
    }

    function getDownlinesAttribute(){
        return Downlines::where('account_id', $this->account->id);
    }

    function getDownlineCountAttribute(){
        return $this->downlines->count();
    }

    function getModulesArrayAttribute(){
        $modules = [];

        $access = ModuleAccess::where('user_id', $this->attributes['id'])->get();

        foreach ($access as $module) {
            $modules[] = $module->module_name;
        }

        return $modules;
    }

}
