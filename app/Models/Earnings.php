<?php namespace App\Models;

class Earnings extends AbstractLayer {

	protected $table = 'earnings';

    protected $appends = [
        'account',
        'user',
        'sourceLabel'
    ];

    protected static function boot(){
        $countryCode = config('system.countryCode');
        $timeZones = \DateTimeZone::listIdentifiers(\DateTimeZone::PER_COUNTRY, $countryCode);
        date_default_timezone_set($timeZones[0]);
        static::saving(function($model){
            $model->created_at = date('Y-m-d h:i:s');
        });
    }

    function pairing(){
        return $this->hasOne($this->namespace.'\Pairing', 'id', 'pairing_id');
    }

    function getAccountAttribute(){
        return Accounts::find($this->attributes['account_id']);
    }

    function getUserAttribute(){
        return User::find($this->attributes['user_id']);
    }

    function getSourceLabelAttribute(){
        $label = str_replace('_', ' ', $this->attributes['source']);

        return ucwords( ($this->attributes['source'] == 'pairing' and $this->attributes['amount'] == 0) ? 'flush out' : $label );
    }

}
