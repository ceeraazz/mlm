<?php namespace App\Models;

class Products extends AbstractLayer {

	protected $table = 'products';

    protected $appends = [
        'purchaseValue'
    ];

    function unilevel(){

        return $this->hasMany($this->namespace . '\ProductUnilevel', 'product_id', 'id');

    }

    function getPurchaseValueAttribute(){

        $charge = (int)config('system.purchase_codes_charge');

        $price = $this->attributes['price'];

        if ($charge > 0){
            $price = $price + (($charge/100)*$price);
        }

        return $price;

    }

}
