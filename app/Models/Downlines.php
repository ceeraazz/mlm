<?php
/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 3/4/17
 * Time: 11:11 AM
 */

namespace App\Models;

class Downlines extends AbstractLayer{

    protected $table = 'downlines';

    function account(){
        return $this->hasOne($this->namespace. '\Accounts', 'id', 'account_id');
    }

}