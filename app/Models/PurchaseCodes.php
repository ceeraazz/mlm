<?php namespace App\Models;

class PurchaseCodes extends AbstractLayer {

	protected $table = 'product_purchase_codes';

    protected $appends = ['theStatus'];

    function product(){
        return $this->hasOne($this->namespace.'\Products', 'id', 'product_id');
    }

    function getTheStatusAttribute(){
        return ($this->attributes['status'] == '0') ? 'available' : 'used';
    }

}
