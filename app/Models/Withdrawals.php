<?php
/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 3/7/17
 * Time: 7:41 PM
 */

namespace App\Models;

class Withdrawals extends AbstractLayer{

    protected $table = 'withdrawals';

    function user(){
        return $this->hasOne($this->namespace . '\User', 'id', 'user_id');
    }

}