<?php
/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 4/23/16
 * Time: 3:19 PM
 */

namespace App\Helpers;

use App\Models\ActivationCodes;
use App\Models\Company;
use App\Models\Products;
use App\Models\PurchaseCodes;

class ActivationCodeHelperClass{

    protected $numOfZeros = 5;

    protected $prefixLength = 5;

    protected $toRandomize = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

    protected $patternEveryLetter = 3;

    protected $batchId = 0;

    protected $codeType = 'regular';

    protected $codeFor = 'admin';

    protected $productID = 0;

    protected $ownerID = 0;

    function setNumOfZeros($num){
        $this->numOfZeros = $num;
        return $this;
    }

    private function getNumOfZeros(){
        return $this->numOfZeros;
    }

    function setPrefixLength($num){
        $this->prefixLength = $num;
        return $this;
    }

    private function getPrefixLength(){
        return $this->prefixLength;
    }

    function setPatternEveryLetter($pattern){
        $this->patternEveryLetter = $pattern;
        return $this;
    }

    private function getPatternEveryLetter(){
        return $this->patternEveryLetter;
    }

    function setBatchID($id){
        $this->batchId = $id;
        return $this;
    }

    private function getBatchID(){
        return $this->batchId;
    }

    function setCodeType($type){
        $this->codeType = $type;
        return $this;
    }

    private function getCodeType(){
        return $this->codeType;
    }

    function setCodeFor($codeFor){
        $this->codeFor = $codeFor;
        return $this;
    }

    private function getCodeFor(){
        return $this->codeFor;
    }

    function setProductID($id){
        $this->productID = $id;
        return $this;
    }

    private function getProductID(){
        return $this->productID;
    }

    function setOwnerID($ownerID){
        $this->ownerID = $ownerID;
        return $this;
    }

    function generateCodes($quantity = 1){

        $prefixCount = $this->getPrefixLength();

        $letters = $this->toRandomize;

        $company = Company::find(1);

        $pattern = $this->getPatternEveryLetter();

        $numberOfZeros = $this->getNumOfZeros();

        $batchid = $this->getBatchID();

        $codeType = $this->getCodeType();

        $codeFor = $this->getCodeFor();

        $productID = $this->getProductID();

        $product = Products::find($productID);

        $codes = [];

        $last = ($codeFor == 'members') ? ActivationCodes::orderBy('id', 'desc')->get() : PurchaseCodes::orderBy('id', 'desc')->get();

        $lastId = 1;

        if (!$last->isEmpty()){
            $lastId = $last->first()->id;
        }

        try {

            for ($codeNum = 0; $codeNum < $quantity; $codeNum++) {
                $thisCode = ($codeFor == 'members') ? $company->activation_code_prefix : $company->product_code_prefix;
                $codePattern = 0;
                $i = 0;

                while ($i < $prefixCount) {

                    shuffle($letters);
                    $thisCode .= $letters[0];
                    if ($codePattern >= $pattern) {
                        $thisCode .= rand(20, 1);
                        $codePattern = 0;
                    }
                    $i++;
                    $codePattern++;
                }

                $activation = $thisCode.str_pad($lastId, $numberOfZeros + 5, '0', STR_PAD_LEFT);
                $account = $thisCode.str_pad($lastId, $numberOfZeros, '0', STR_PAD_LEFT);

                $codesValue = ($productID > 0) ? [
                    'product_id'=>$productID,
                    'code'=>strtoupper($activation),
                    'password'=>shortEncrypt($lastId.$productID.time()),
                    'status'=>'0',
                    'created_at' => date('Y-m-d h:i:s')
                ] : [

                    'batch_id'=>$batchid,
                    'code'=>strtoupper($activation),
                    'account_id'=>$account,
                    'status'=>'available',
                    'type'=>$codeType,
                    'user_id'=>($this->ownerID > 0) ? $this->ownerID : '0',
                    'created_at'=>date('Y-m-d h:i:s')

                ];

                if ($codeFor == 'members'){
                    $codesValue['owner_id'] = $this->ownerID;
                    $codesValue['purchase_value'] = $product->purchaseValue;
                }
                $codes[] = $codesValue;


                $lastId++;

            }
        }catch (\Exception $e){
            echo $e->getMessage();
        }

        return $codes;
    }

}