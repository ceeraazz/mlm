<?php
/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 5/26/17
 * Time: 2:28 PM
 */

define('GC_EARNINGS', 'GC');
define('UNILEVEL_EARNINGS', 'unilevel');
define('REBATES_EARNINGS', 'rebates');
define('PAIRING_EARNINGS', 'pairing');
define('DIRECT_REFERRAL_EARNINGS', 'direct_referral');
define('SYSTEM_GENERATED', 'system_generated');
define('FREE_CODE', 'free');
define('REGULAR_CODE', 'regular');
define('USED_STATUS', 'used');

define('REGISTRATION_KEY', 'registration');
define('LOGIN_KEY', 'verification');
define('WITHDRAWAL_KEY', 'withdrawal');

define('USER_FIRST_NAME_KEY', 'USER_FIRST_NAME');
define('USER_LAST_NAME_KEY', 'USER_LAST_NAME');
define('USER_FULL_NAME_KEY', 'USER_FULL_NAME');
define('USER_EMAIL_KEY', 'USER_EMAIL');
define('USER_ACCOUNT_ID_KEY', 'USER_ACCOUNT_ID');
define('USER_LOGIN_USERNAME_KEY', 'USER_LOGIN_USERNAME');
define('USER_WITHDRAWAL_STATUS_KEY', 'USER_WITHDRAWAL_STATUS');

define('MAILING_PREFIX', '{!!');
define('MAILING_SUFFIX', '!!}');

define('ADMIN_ROLE', 'admin');
define('MEMBER_ROLE', 'member');

define('APPROVED_STATUS', 'approved');
define('DENIED_STATUS', 'denied');

define('ALL_TIME_KEY', 'all-time');

define('DASHBOARD_MODULE', 'dashboard');
define('ACTIVATION_CODES_MODULE', 'activation-codes');
define('MEMBERS_MODULE', 'members');
define('FUNDING_MODULE', 'funding');
define('TRANSACTIONS_MODULE', 'transactions');
define('WITHDRAWALS_MODULE', 'withdrawals');
define('TOP_EARNERS_MODULE', 'top-earners');
define('ADMINISTRATORS_MODULE', 'administrators');
define('PRODUCTS_MODULE', 'products');
define('PURCHASE_CODES_MODULE', 'purchase-codes');
define('UNILEVEL_MODULE', 'unilevel');
define('COMPANY_DETAILS_MODULE', 'details');
define('CONNECTIONS_MODULE', 'connections');
define('CODE_SETTINGS_MODULE', 'code-settings');
define('COMPENSATION_INCOME_MODULE', 'income');
define('PAIRING_MODULE', 'pairing');
define('WITHDRAWAL_SETTINGS_MODULE', 'withdrawal-settings');
define('MAIL_TEMPLATES_MODULE', 'mail-templates');
define('LEFT_NODE', 'left');
define('RIGHT_NODE', 'right');
define('DEFAULT_SPONSOR_ID', 1);
define('TRUE_STATUS', 'true');
define('FALSE_STATUS', 'false');
define('YES_STATUS', 'yes');
define('NO_STATUS', 'no');
define('PENDING_STATUS', 'pending');
define('PAYMENT_HISTORY_MODULE', 'payment-history');
define('PAYPAL_MODULE', 'paypal');
define('CREATED_AT_FORMAT', 'Y-m-d H:i:s');