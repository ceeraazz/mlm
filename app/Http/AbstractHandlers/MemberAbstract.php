<?php
/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 3/4/17
 * Time: 6:30 PM
 */

namespace App\Http\AbstractHandlers;

use App\Helpers\ActivationCodeHelperClass;
use App\Helpers\ModelHelper;
use App\Http\TraitLayer\GlobalSettingsTrait;
use App\Models\ActivationCodes;
use App\Models\Company;
use App\Models\ReactivationPurchases;
use Illuminate\Support\Facades\Auth;

abstract class MemberAbstract extends MainAbstract{

    use GlobalSettingsTrait;

    protected $viewpath = '';
    protected $theUser;
    protected $theCompany;
    protected $batchID = 0,
        $type = 'regular',
        $numberOfZeros = 5,
        $patternEveryLetter = 3,
        $prefixLength = 5,
        $codesToGenerate = 1
    ;

    function __construct(){
        parent::__construct();
        $this->theUser = Auth::user();
        $this->theCompany = Company::find($this->companyID);

        view()->share([
            'theUser'=>$this->theUser,
            'company'=>$this->theCompany,
            'menus'=>$this->memberMenus
        ]);

        if ($this->theCompany->activation_every > 0){
            $this->checkIfNeedsActivation();
        }

    }

    private function checkIfNeedsActivation(){

        $overallEarnings = $this->theUser['earnings'];
        $purchasedReactivation = ReactivationPurchases::where([
            'user_id'=>$this->theUser['id']
        ])->get();
        $targetNumberOfActivation = (int)($overallEarnings / $this->theCompany->activation_every);

        if ($targetNumberOfActivation > $purchasedReactivation->count()){
            $this->theUser->is_maintained = false;
            $this->theUser->save();
        }

    }

    function generateActivationCode(){
        $codes = new ActivationCodeHelperClass();
        $codes
            ->setBatchID($this->batchID)
            ->setCodeType($this->type)
            ->setNumOfZeros($this->numberOfZeros)
            ->setPatternEveryLetter($this->patternEveryLetter)
            ->setPrefixLength($this->prefixLength)
            ->setOwnerID($this->theUser->id);

        $theCodes = $codes->generateCodes($this->codesToGenerate);

        $model = new ModelHelper();
        $theCodes[0]['paid_by_balance'] = true;
        return $model->manageModelData(new ActivationCodes, $theCodes[0]);
    }

}