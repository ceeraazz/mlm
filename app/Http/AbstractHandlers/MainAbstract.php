<?php namespace App\Http\AbstractHandlers;


use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Details;
use App\Models\Earnings;
use App\Models\PairingSettings;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

abstract class MainAbstract extends Controller{

    public $request;
    protected $records_per_page = 20;

    protected $rebatesEarningKey = 'rebates';
    protected $unilevelEarningKey = 'unilevel';

    protected $moneyPotProductPurchase = 'product_purchase';

    protected $upload_location = 'public/uploads';

    protected $createdAtFormat = 'Y-m-d H:i:s';

    protected $systemConfig = 'config/system.php';

    protected $currencies = [
        'usd'=>'&#36;',
        'yen'=>'&#xa5;',
        'eur'=>'&#x80;',
        'php'=>'&#x20b1;',
    ];

    protected $adminMenus = [
            'dashboard'=>[
                'activation-codes',
                'members',
                'funding',
                'transactions',
                'withdrawals',
                'top-earners',
                'administrators',
                'payment-history',
            ],
            'products'=>[
                'purchase-codes',
                'unilevel'
            ],
            'company'=>[
                'details',
                'settings',
                'connections',
                'registration',
                'paypal'
            ],
            'compensation-settings'=>[
                'income',
                'pairing',
                'vouchers',
                'withdrawal-settings'
            ],
            'mail-settings'=>[
                'mail-templates',
            ]
        ];

    protected $memberMenus = [
        'dashboard'=>[
            'investments',
            'network-tree',
        ],
        'purchases'=>[
            'buy',
            'encode',
            'history',
        ],
        'withdrawals'=>[
            'request',
            'pending',
            'history',
        ],
    ];

    function __construct(){
        if (!file_exists($this->upload_location)){
            mkdir($this->upload_location, 0777, true);
        }
    }

    function formatException($e){

        return sprintf('%s on line %s of file %s', $e->getMessage(), $e->getLine(), $e->getFile());

    }

    function validateUserDetails($request, $inject_rules = [], $requireBasicFields = true){

        $rules = ($requireBasicFields) ? [
            'first_name'=>'required',
            'last_name'=>'required',
        ] : [];

        /*if (isEmailRequired()){
            $rules['email'] = 'required|email|is_unique:user_details,email';
        }*/

        if (count($inject_rules) > 0){
            $rules = array_merge($rules, $inject_rules);
        }

        if ($request->password != null){
            $rules['password'] = 'min:8';
            $rules['password_confirm'] = 'required|same:password';
        }

        $validation = Validator::make($request->input(), $rules);

        return $validation;
    }

    function saveDetails($user_id, $request){
        $user = User::find($user_id);
        $details = Details::find($user->details->id);
        $details->first_name = $request->first_name;
        $details->last_name = $request->last_name;
        $details->bank_name = $request->bank_name;
        $details->email = $request->email;
        $details->account_name = $request->bank_account_name;
        $details->account_number = $request->bank_account_number;
        $user->username = $request->username;
        if ($request->password !== '') {
            $user->password = Hash::make($request->password);
        }
        $details->save();
        $user->save();
    }

    function getEarningsDates(){
        $dates = Earnings::orderBy('id', 'DESC')->get();

        $theDates = [];
        $encoded = [];

        foreach ($dates as $date){
            $timestamp = strtotime($date->created_at);
            $value = sprintf('%s-%s', date('Y', $timestamp), date('m', $timestamp));

            if (!in_array($value, $encoded)){
                $theDates[$value] = sprintf('%s %s', date('F', $timestamp), date('Y', $timestamp));
                $encoded[] = $value;
            }

        }

        return $theDates;
    }

}