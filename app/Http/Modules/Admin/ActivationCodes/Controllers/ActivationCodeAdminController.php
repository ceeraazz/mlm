<?php namespace App\Http\Modules\Admin\ActivationCodes\Controllers;

use App\Http\AbstractHandlers\AdminAbstract;
use App\Http\Modules\Admin\ActivationCodes\Validation\ActivationCodeValidationHandler;
use App\Http\Requests;
use App\Models\ActivationCodeBatches;
use App\Models\ActivationCodes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ActivationCodeAdminController extends AdminAbstract {

    protected $viewpath = 'Admin.ActivationCodes.views.';

    function __construct(){
        parent::__construct();
    }

    function getIndex(){
        return view($this->viewpath.'index')
            ->with(
                [
                    'batches'=>ActivationCodeBatches::paginate(50)
                ]
            );
    }

    function postIndex(Request $request){
        $codeValidator = new ActivationCodeValidationHandler();
            $codeValidator->setInputs($request->input());

        $result = $codeValidator->validate();

        Session::flash($result->message_type, $result->message);

        return ($result->error) ? redirect('admin/activation-codes')->withErrors($result->validation->errors())->withInput() : redirect('admin/activation-codes/view-batch/'.$result->batch_id);
    }

    function getViewBatch($id = null){
        return view($this->viewpath.'view-codes')->with(
            [
                'codes'=>ActivationCodes::where('batch_id', $id)->paginate(50)
            ]
        );
    }

}
