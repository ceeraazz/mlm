<?php

namespace App\Http\Modules\Admin\Pairing\Controllers;

use App\Http\AbstractHandlers\AdminAbstract;
use App\Models\Company;
use App\Models\PairingSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class PairingAdminController extends AdminAbstract{

    protected $viewpath = 'Admin.Pairing.views.';

    function __construct(){
        parent::__construct();
    }

    function getIndex(){
        return view( $this->viewpath . 'index' );
    }

    function postIndex(Request $request){

        $result = new \stdClass();
        $result->error = false;
        $result->message = '';

        $company = $this->theCompany;

        $validation = Validator::make([], []);

        if($request->save_settings){

            $validation = Validator::make($request->input(), [
                'max_pair'=>'numeric',
                'referral_income'=>'numeric',
                'pairing_income'=>'required|numeric'
            ]);


            if ($validation->fails()) {
                $result->error = true;
            } else {
                DB::beginTransaction();
                try {
                    $company->daily_max_pair = $request->max_pair;
                    $company->enable_flush_out = $request->flush_out;
                    $company->referral_income = $request->referral_income;
                    $company->pairing = $request->pairing_income;
                    $company->save();
                    $result->message = Lang::get('pairing.settings_saved');
                    DB::commit();

                } catch (\Exception $e) {
                    DB::rollback();
                    $result->error = true;
                    $result->message = $this->formatException($e);
                }
            }


        } else if($request->save_money_pot){

            DB::beginTransaction();
            try {
                $company = Company::find(1);
                $company->money_pot = $request->money_pot_amount;
                $company->save();
                $result->message = Lang::get('pairing.money_pot_saved');
                DB::commit();
            } catch (\Exception $e){
                DB::rollback();
                $result->error = true;
                $result->message = $this->formatException($e);
            }

        }

        return ($result->error) ? redirect('admin/pairing')->with('danger', $result->message)->withErrors($validation->errors()) : redirect('admin/pairing')->with('success', $result->message);

    }

}