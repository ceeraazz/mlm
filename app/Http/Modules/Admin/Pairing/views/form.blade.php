<div class="row">
    <div class="col-md-12">
        <div class="panel panel-theme rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">{{ Lang::get('pairing.settings') }}<code></code></h3>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body no-padding" style="display: block;">

                {{ Form::open([ 'class'=>'form-inline' ]) }}
                <div class="form-body">
                    <div class="form-group pull-left col-md-3">
                        {{ validationError($errors, 'pairing_income') }}
                        <label class="col-sm-12 control-label">{{ Lang::get('pairing.income') }}</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" name="pairing_income" value="{{ old('pairing_income', $company->pairing) }}">
                        </div>
                    </div>
                    <div class="form-group pull-left col-md-3">
                        {{ validationError($errors, 'referral_income') }}
                        <label class="col-sm-12 control-label">{{ Lang::get('pairing.referral_income') }}</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" name="referral_income" value="{{ old('max_pair', $company->referral_income) }}">
                        </div>
                    </div>
                    <div class="form-group pull-left col-md-3">
                        {{ validationError($errors, 'max_pair') }}
                        <label class="col-sm-12 control-label">{{ Lang::get('pairing.max_pair') }}</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" name="max_pair" value="{{ old('max_pair', $company->daily_max_pair) }}">
                        </div>
                    </div>
                    <div class="form-group pull-left col-md-3">
                        <label class="col-sm-12 control-label">{{ Lang::get('pairing.flush_out') }}</label>
                        <div class="col-sm-12">
                            {{ Form::select('flush_out', [
                                '0'=>Lang::get('pairing.enable'),
                                '1'=>Lang::get('pairing.disable')
                            ], old('flush_out', $company->enable_flush_out), [
                                'class'=>'form-control chosen-select'
                            ]) }}

                        </div>
                    </div>
                    <br/>
                    <div class="form-group col-xs-12 col-md-12">
                        <button type="submit" name="save_settings" value="save_settings" class="btn btn-success">{{ Lang::get('labels.save') }}</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>

</div>