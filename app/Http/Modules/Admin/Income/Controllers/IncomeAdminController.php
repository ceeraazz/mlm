<?php
/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 3/3/17
 * Time: 11:35 PM
 */

namespace App\Http\Modules\Admin\Income\Controllers;

use App\Http\AbstractHandlers\AdminAbstract;
use App\Models\Company;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class IncomeAdminController extends AdminAbstract{

    protected $viewpath = 'Admin.Income.views.';

    function __construct(){
        parent::__construct();
    }

    function getIndex(){

        return view( $this->viewpath . 'index' );

    }

    function postIndex(Request $request){
        $result = new \stdClass();
        $result->error = false;
        $result->message = '';
        $company = Company::find(1);

        if($request->save_entry_fee){

            DB::beginTransaction();
            try {
                $company->money_pot = $request->money_pot_amount;
                $company->entry_fee = $request->entry_fee;
                $company->save();
                $result->message = Lang::get('messages.saved');
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $result->error = true;
                $result->message = $this->formatException($e);

                if ($e instanceof QueryException) {
                    $result->message = Lang::get('messages.generic_error');
                }
            }


        } else if ($request->save_maintenance){
            $company->minimum_product_purchase = $request->minimum_purchase;
            $company->save();
            $result->message = Lang::get('messages.saved');

        }
        return ($result->error) ? redirect('admin/income')->with('danger', $result->message) : redirect('admin/income')->with('success', $result->message);

    }

}