<?php
/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 3/4/17
 * Time: 6:20 PM
 */

namespace App\Http\Modules\Admin\Withdrawals\Controllers;

use App\Helpers\MailHelper;
use App\Http\AbstractHandlers\AdminAbstract;
use App\Models\Withdrawals;
use Illuminate\Support\Facades\Lang;

class WithdrawalsAdminController extends AdminAbstract{

    protected $viewpath = 'Admin.Withdrawals.views.';

    function __construct(){
        parent::__construct();
    }

    function getIndex(){

        return view( $this->viewpath . 'index' )
            ->with([
                'withdrawals'=>Withdrawals::orderBy('id', 'desc')->paginate($this->records_per_page)
            ]);

    }

    function getCancelRequest($id){
        $withdraw = Withdrawals::find($id);

        if (isset($withdraw->id)) {
            $withdraw->status = DENIED_STATUS;
            $withdraw->save();

            if ($withdraw->user->details->email != null and isEmailRequired()){
                $mail = new MailHelper();
                $mail->setUserObject($withdraw->user)
                    ->setWithdrawalObject($withdraw)
                    ->sendMail(WITHDRAWAL_KEY);
            }
        }

        return back()->with('success', Lang::get('withdrawal.success_deny'));;
    }

    function getApproveRequest($id)
    {
        $withdraw = Withdrawals::find($id);

        if (isset($withdraw->id)) {
            $withdraw->status = APPROVED_STATUS;
            $withdraw->save();

            if ($withdraw->user->details->email != null and isEmailRequired()){
                $mail = new MailHelper();
                $mail->setUserObject($withdraw->user)
                    ->setWithdrawalObject($withdraw)
                    ->sendMail(WITHDRAWAL_KEY);
            }
        }

        return back()->with('success', Lang::get('withdrawal.success_approved'));;
    }

}