@extends('layouts.master')
@section('content')

    {{ view('widgets.withdrawals.table_widget')
     ->with([
        'withdrawals'=>$withdrawals
     ])->render() }}

    <center>{{ $withdrawals->render() }}</center>

@stop