<?php
/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 3/7/17
 * Time: 8:05 PM
 */

namespace App\Http\Modules\Member\NetworkTree\Controllers;

use App\Helpers\Binary;
use App\Http\AbstractHandlers\MemberAbstract;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NetworkTreeMemberController extends MemberAbstract{

    protected $viewpath = 'Member.NetworkTree.views.';

    function __construct(){
        parent::__construct();
    }

    function getIndex(Request $request, $start = null){

        if (!isset($this->theUser->account->id)){
            return view('widgets.activate_account');
        }

        $binary = new Binary();
        $recorded = (session('recorded_viewing')) ? session('recorded_viewing') : [];

        $level1Account = $this->theUser->account->id;
        $currentUser = $this->theUser;
        if ($start != null){
            $exp = explode('-', $start);
            $userID = $exp[1];
            $user = User::find($userID);
            $currentUser = $user;
            $level1Account = $user->account->id;
            if (!in_array($userID, $recorded)){
                $recorded[] = $userID;
                session(['recorded_viewing'=>$recorded]);
            } else {

                $newRecord = [];
                foreach ($recorded as $key=>$value){
                    if ($value == $userID){
                        $newRecord[] = $userID;
                        break;
                    } else {
                        $newRecord[] = $userID;
                    }
                }

                session(['recorded_viewing'=>$newRecord]);

            }
        } else {
            Session::forget('recorded_viewing');
        }

        $listedUser = [];
        if (count($recorded) > 0){
            $usersQuery = User::whereIn('id', $recorded)->get();
            foreach ($usersQuery as $user){
                $listedUser[$user->id] = $user;
            }
        }

        return view( $this->viewpath . 'index' )
            ->with([
                'binaryTree'=>$binary->setLevelOneAccount($level1Account)->renderTree(),
                'viewed'=>session('recorded_viewing'),
                'listedUser'=>$listedUser,
                'currentUser'=>$currentUser
            ]);

    }

}