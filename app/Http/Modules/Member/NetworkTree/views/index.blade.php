@extends('layouts.members')
@section('content')

    @if (count($viewed) > 0)
        <div class="">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ url('member/network-tree') }}">You : {{ $theUser->account->code->account_id }}</a>
                </li>
                @foreach ($viewed as $userID)
                    <li>
                        <a href="{{ url(sprintf('member/network-tree/index/%s-%s', strtoupper($listedUser[$userID]->account->code->account_id), $userID)) }}">{{ strtoupper($listedUser[$userID]->account->code->account_id) }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    {{ Html::style('public/plugins/jquery-orgchart/jquery.orgchart.css') }}
    <ul id="chart" class="hidden">
        <li class="binary code-{{ $currentUser->account->type or 'free' }}">
            <img src="{{ url($currentUser->details->thePhoto) }}" width="80" height="80">
            <span class="owner-name">{{ $currentUser->details->fullName }}</span>
            <span class="account-id">{{ $currentUser->account->code->account_id }}</span>
            <ul>
                {{ $binaryTree }}
            </ul>

        </li>
    </ul>
    <div id="main"></div>

@stop

@section('custom_includes')
    {{ Html::script('public/plugins/jquery-orgchart/jquery.orgchart.min.js') }}
    <script type="text/javascript">
        $("#chart").orgChart({container: $("#main")});
    </script>
@stop