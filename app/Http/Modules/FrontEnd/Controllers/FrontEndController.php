<?php namespace App\Http\Modules\FrontEnd\Controllers;

use App\Helpers\MailHelper;
use App\Helpers\ShortEncrypt;
use App\Http\AbstractHandlers\MainAbstract;
use App\Http\Modules\FrontEnd\Validation\FrontEndValidationHandler;
use App\Http\TraitLayer\BinaryTrait;
use App\Models\Accounts;
use App\Models\ActivationCodes;
use App\Models\Details;
use App\Models\User;
use App\Models\Withdrawals;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class FrontEndController extends MainAbstract {

    use BinaryTrait;

    protected $viewPath = 'FrontEnd.views.';

    function getLogin(){
        return view($this->viewPath.'login');
    }

    function postLogin(Request $request){

        $validate = new FrontEndValidationHandler();
            $validate
                ->setInputs($request->input());

        $result = $validate->validate();

        Session::flash($result->message_type, $result->message);

        if (!$result->error) {
            $role = Auth::user()->role;
        }

        $nextUrl = null;
        if (!$result->error){
            $user = Auth::user();
            if ($user->details->email != null and isEmailRequired() and $user->role == MEMBER_ROLE){
                $code = ShortEncrypt::make(time());
                $user->verification_code = $code;
                $user->save();
                $nextUrl = 'auth/verify';
                $mail = new MailHelper();
                $mail->setUserObject($user)->setVerificationCode($code);
                $mail->sendMail(LOGIN_KEY);
            } else {
                $nextUrl = sprintf('%s/dashboard', $role);
            }
        }

        return ($result->error) ? redirect('auth/login') : redirect($nextUrl);

    }

    function getSignUp(Request $request){
        return view( $this->viewPath . 'sign_up' )
            ->with([
                'id'=>0,
                'user'=>User::find(0),
                'referral'=>$request->ref,
                'suffix'=>($request->ref != null) ? '?ref=' . $request->ref : null
            ]);
    }

    function postSignUp(Request $request){

        $result = new \stdClass();
        $result->error = false;
        $result->message = '';

        $additional_rules = [
            'bank_name'=>'required',
            'bank_account_name'=>'required',
            'bank_account_number'=>'required',
            'password'=>'required|min:8',
            'password_confirm'=>'same:password|required',
            'first_name'=>'required',
            'last_name'=>'required',
            'username'=>'required|unique:users'
        ];

        if ($request->have_activation == YES_STATUS){
            $additional_rules['upline_id'] = 'required';
            $additional_rules['account_id'] = 'required';
            $additional_rules['activation_code'] = 'required';
        }

        if (isEmailRequired()){
            $additional_rules['email'] = 'required|email|unique:user_details,email';
        }

        $validation = Validator::make($request->input(), $additional_rules);

        if ($validation->fails()){
            $result->error = true;
        }

        if ($request->have_activation == YES_STATUS){
            $searchUpline = ActivationCodes::where([
                'account_id'=>$request->upline_id,
                'status'=>'used'
            ])->get();

            $searchSponsor = ActivationCodes::where([
                'account_id'=>$request->sponsor_id,
                'status'=>'used'
            ])->get();

            $checkActivation = ActivationCodes::where([
                'account_id'=>$request->account_id,
                'code'=>$request->activation_code,
                'status'=>'available'
            ])->get();

            $account = (!$searchUpline->isEmpty()) ? Accounts::where('code_id', $searchUpline->first()->id)->get()->first() : null;

            $sponsorAccountId = (!$searchSponsor->isEmpty()) ? Accounts::where('code_id', $searchSponsor->first()->id)->get()->first()->id : null;

            $uplineDownlineCount = (!$searchUpline->isEmpty()) ? Accounts::where('upline_id', $account->id)->count() : 0;

            if ($validation->fails()){
                $result->error = true;
            } else if ($checkActivation->isEmpty()){

                $result->message = Lang::get('messages.invalid_activation_code');
                $result->error = true;

            } else if($searchUpline->isEmpty()) {

                $result->error = true;
                $result->message = Lang::get('messages.invalid_upline');

            } else if ($uplineDownlineCount >= 2){

                $result->error = true;
                $result->message = Lang::get('messages.upline_has_complete_nodes');

            } else if(Accounts::where('node', $request->node_placement)->where('upline_id', $account->id)->count() > 0 ){

                $result->message = Lang::get('messages.node_already_occupied');
                $result->error = true;

            }
        }

        if (!$result->error) {
            try {
                $details = new Details();
                $details->first_name = $request->first_name;
                $details->last_name = $request->last_name;
                $details->email = $request->email;
                $details->save();

                $user = new User();
                $user->username = $request->username;
                $user->password = Hash::make($request->password);
                $user->user_details_id = $details->id;
                $user->paid = ($request->have_activation == YES_STATUS) ? TRUE_STATUS : FALSE_STATUS;
                $user->role = MEMBER_ROLE;
                $user->save();

                $this->saveDetails($user->id, $request);

                if ($request->have_activation == YES_STATUS){
                    $result = $this
                        ->setUserObject($user)
                        ->setSponsorId($sponsorAccountId)
                        ->setActivationCodeId($checkActivation->first()->id)
                        ->setUplineId($account->id)
                        ->validateAdminRegistration($request);
                }

                $mail = new MailHelper();
                $mail->setUserObject($user);
                $mail->sendMail(REGISTRATION_KEY);

                if (!isset(Auth::user()->id)) {
                    //if no one is logged in, then auto sign in
                    Auth::loginUsingId($user->id);
                    return redirect('member/dashboard');
                }

            } catch (\Exception $e) {
                $result->error = true;
                $result->message = $this->formatException($e);
            }
        }

        $suffix = ($request->ref != null) ? '?ref=' . $request->ref : null;

        return ($result->error) ? redirect(sprintf('auth/sign-up%s', $suffix))
            ->withInput()
            ->withErrors($validation->errors())
            ->with('danger', $result->message) : redirect('auth/login')
            ->with('success', $result->message);

    }

    function getVerify(){
        if (!Auth::check()){
            return redirect('auth/login');
        }

        return view($this->viewPath . 'verify');
    }

    function postVerify(Request $request){
        $user = Auth::user();

        if ($request->verification_code != $user->verification_code){
            return back()->with('danger', Lang::get('messages.invalid_code'));
        } else {
            $user->verification_code = '';
            $user->save();
            return redirect('member/dashboard');
        }
    }

    function getTestMail(){
        $mail = new MailHelper();
        $mail->setPreview(true)->setUserObject(User::find(2))->setWithdrawalObject(Withdrawals::find(1));
        return $mail->sendMail(WITHDRAWAL_KEY);
    }

}
