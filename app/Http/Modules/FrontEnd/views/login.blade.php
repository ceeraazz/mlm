@extends('layouts.loginLayout')
@section('content')
    <div id="sign-wrapper">
        @php die("Sdf"); @endphp
        {{ BootstrapAlert() }}

        <!-- Login form -->
        {{ Form::open(
            [
                'class'=>'sign-in form-horizontal shadow rounded no-overflow'
            ]
        ) }}
            <div class="sign-header">
                <div class="form-group">
                    <div class="sign-text">
                        <span>{{ Lang::get('labels.members_area') }}</span>
                    </div>
                </div><!-- /.form-group -->
            </div><!-- /.sign-header -->
            <div class="sign-body">
                <div class="form-group">
                    <div class="input-group input-group-lg rounded no-overflow {{ $errors->first('email') ? 'has-error has-feedback' : null }}">
                        <input type="text" class="form-control input-sm" placeholder="{{ Lang::get('labels.username') }}" name="email">
                        {{ validationError($errors, 'email') }}
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                </div><!-- /.form-group -->
                <div class="form-group">
                    <div class="input-group input-group-lg rounded no-overflow">
                        <input type="password" class="form-control input-sm" placeholder="{{ Lang::get('labels.password') }}" name="password">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    </div>
                </div><!-- /.form-group -->
            </div><!-- /.sign-body -->
            <div class="sign-footer">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="ckbox ckbox-theme">
                                <input id="rememberme" name="remember_me" type="checkbox">
                                <label for="rememberme" class="rounded">{{ Lang::get('labels.remember_me') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded spinner" id="login-btn">{{ Lang::get('labels.sign_in') }}</button>
                </div>
            </div>
        {{ Form::close() }}
        <!--/ Login form -->

        <!-- Content text -->
        <p class="text-muted text-center sign-link">{{ Lang::get('labels.not_yet_registered') }} <a href="{{ url('auth/sign-up') }}"> {{ Lang::get('labels.sign_up') }}</a></p>
        <!--/ Content text -->

    </div><!-- /#sign-wrapper -->
@stop

@section('pageIncludes')
    <script src="{{ url('assets/global/plugins/bower_components/jquery-validation/dist/jquery.validate.min.js') }}"></script>
@stop