<?php
/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 3/3/17
 * Time: 11:32 AM
 */

return [

    'photo'=>'Photo',
    'name'=>'Name',
    'username'=>'Username',
    'earnings'=>'Earnings',
    'earned'=>'Earned',
    'email'=>'Email',
    'downline_left'=>'Downline Left',
    'downline_right'=>'Downline Right',
    'accounts_owned'=>'Accounts Owned',
    'id'=>'Member ID',
    'update_details'=>'Update Details of',
    'add_new'=>'Add New Member',
    'first_name'=>'First Name',
    'last_name'=>'Last Name',
    'change_password'=>'Change Password',
    'new_password'=>'New Password',
    'select_code'=>'Select Code',
    'node_placement'=>'Node Placement',
    'select_upline'=>'Select Upline',
    'select_sponsor'=>'Select Sponsor',

    'no_records'=>'No members yet.',
    'invalid_admin_password'=>'Invalid Administrator password',
    'updated'=>'User credential successfully updated.',
    'select_activation_code'=>'Select Activation Code',

    'bank_name'=>'Bank Name',
    'bank_account_name'=>'Account Name',
    'bank_account_number'=>'Account Number',
    'confirm_new_password'=>'Confirm Password',
    'referral_link'=>'Referral Link',
    'direct_referral'=>'Direct Referral',
    'upline'=>'Upline',
    'have_activation'=>'Got Activation Code?',
    'account_inactive'=>'Your account isn\'t active yet.',
    'activation_message'=>'You can either pay via PayPal, Bank, or Directly to the Administrator to activate your account.'

];