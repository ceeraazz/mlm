<?php
/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 3/7/17
 * Time: 7:53 PM
 */

return [

    'account_id'=>'Account ID',
    'sponsor'=>'Sponsor',
    'upline'=>'Upline',
    'node'=>'Node',
    'available_balance'=>'Available Balance',
    'overall_income'=>'Overall Income',
    'earned'=>'Earned',
    'buy'=>'Buy',
    'success_buy'=>'You have purchased investment account.',
    'low_balance'=>'Sorry, you don\'t have enough balance to buy another slot.',
    'purchased'=>'Purchased Slots'

];