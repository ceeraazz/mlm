<table class="table table-bordered table-hover table-striped">
    <thead>
        @if ($theUser->role == 'admin')
            <th>{{ Lang::get('withdrawal.requested_by') }}</th>
        @endif
        <th>{{ Lang::get('withdrawal.amount') }}</th>
        <th>{{ Lang::get('withdrawal.bank_details') }}</th>
        <th>{{ Lang::get('withdrawal.notes') }}</th>
        <th>{{ Lang::get('withdrawal.status') }}</th>
        <th>{{ Lang::get('withdrawal.action') }}</th>
    </thead>
    <tbody>
    @if ($withdrawals->count() > 0)
        @foreach($withdrawals as $row)
            <?php
                $tax = $company->withdrawalSettings->tax_percentage;
                $adminFee = $company->withdrawalSettings->admin_charge;
                $totalTax = calculatePercentage($tax, $row->amount);

                switch($row->status){
                    case 'denied':
                        $badge = 'label-danger';
                        break;

                    case 'approved':
                        $badge = 'label-success';
                        break;

                    default:
                        $badge = 'label-warning';
                }
            ?>
            <tr>
                @if ($theUser->role == 'admin')
                    <td>{{ $row->user->details->fullName }}</td>
                @endif
                <td>
                    <table>
                        <tr>
                            <td>{{ Lang::get('withdrawal.requested_amount') }}</td>
                            <td>: <b>{{ number_format($row->amount, 2) }}</b></td>
                        </tr>
                        <tr>
                            <td>{{ Lang::get('withdrawal.tax') }}</td>
                            <td>: <b style="color:red">-{{ number_format($totalTax, 2) }}</b></td>
                        </tr>
                        <tr>
                            <td>{{ Lang::get('withdrawal.admin_fee') }}</td>
                            <td>: <b style="color:red">-{{ number_format($adminFee, 2) }}</b></td>
                        </tr>
                        <tr>
                            <td style="border-top:1px solid gray;">{{ Lang::get('withdrawal.net') }}</td>
                            <td style="border-top:1px solid gray;">: <b style="color:green; font-size:18px">{{ number_format($row->amount - ($totalTax + $adminFee), 2) }}</b></td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>{{ Lang::get('labels.bank_name') }}</td>
                            <td>: <b>{{ $row->bank_name }}</b></td>
                        </tr>
                        <tr>
                            <td>{{ Lang::get('labels.bank_account_name') }}</td>
                            <td>: <b>{{ $row->account_name }}</b></td>
                        </tr>
                        <tr>
                            <td>{{ Lang::get('labels.bank_account_number') }}</td>
                            <td>: <b>{{ $row->account_number }}</b></td>
                        </tr>
                    </table>
                </td>
                <td>{{ $row->notes }}</td>

                <td>{{ sprintf('<span class="label %s">%s</span>', $badge, strtoupper($row->status)) }}</td>
                <td>
                    @if ($row->status == 'pending')
                        <a href="{{ url(Request::segment(1).'/withdrawals/cancel-request/'.$row->id) }}" class="btn btn-warning btn-xs">{{ Lang::get('withdrawal.cancel_request') }}</a>

                    @if ($theUser->role == 'admin')
                            <a href="{{ url(Request::segment(1).'/withdrawals/approve-request/'.$row->id) }}" class="btn btn-success btn-xs">{{ Lang::get('withdrawal.approve_request') }}</a>
                        @endif
                    @endif
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="10"><center>{{ Lang::get('withdrawal.no_request') }}</center></td>
        </tr>
    @endif
    </tbody>
</table>