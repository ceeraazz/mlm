<table class="table table-bordered table-striped">
    <thead>
    <th>{{ Lang::get('codes.activation') }}</th>
    <th>{{ Lang::get('codes.account_id') }}</th>
    <th>{{ Lang::get('codes.status') }}</th>
    <th>
        @if (Request::segment(1) != 'member')
            {{ Lang::get('codes.type') }}
        @else
            {{ Lang::get('labels.action') }}
        @endif
    </th>
    </thead>
    <tbody>
    @if ($codes->isEmpty())
        <tr>
            <td colspan="10">
                <center>
                    <i>{{ Lang::get('codes.no_record') }}</i>
                </center>
            </td>
        </tr>
    @else
        @foreach($codes as $code)
            <tr>
                <td>{{ $code->code }}</td>
                <td>{{ $code->account_id }}</td>
                <td>{{ ucwords($code->status) }}</td>
                <td>
                    @if (Request::segment(1) != 'member')
                        {{ ucwords($code->type) }}
                    @else
                        @if ($code->status == 'available')
                            <a class="btn btn-warning btn-xs" href="{{ url('member/investments/encode') }}">{{ Lang::get('codes.encode') }}</a>
                        @endif
                    @endif
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>