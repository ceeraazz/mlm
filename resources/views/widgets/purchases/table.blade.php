
<table class="table table-bordered table-hover">
    <thead>
        <th>{{ Lang::get('products.name') }}</th>
        <th>{{ Lang::get('products.amount') }}</th>
        <th>{{ Lang::get('products.rebates') }}</th>
    </thead>
    <tbody>
    <?php
        $purchaseTotal = 0;
        $rebatesTotal = 0;
    ?>
        @foreach ($purchases as $row)
            <tr>
                <td>{{ $row->product->name }}</td>
                <td>{{ number_format($row->product->price, 2) }}</td>
                <td>{{ ($row->rebatesDetails != null) ? number_format($row->rebatesDetails->amount, 2) : 0; }}</td>
            </tr>
            <?php
                $purchaseTotal += $row->product->price;
                $rebatesTotal += ($row->rebatesDetails != null) ? $row->rebatesDetails->amount : 0;
            ?>
        @endforeach
        <tr class="success">
            <td><center>{{ Lang::get('products.total') }}</center></td>
            <td>{{ number_format($purchaseTotal, 2) }}</td>
            <td>{{ number_format($rebatesTotal, 2) }}</td>
        </tr>
    </tbody>
</table>