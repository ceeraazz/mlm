<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- START @HEAD -->
<head>
    <!-- START @META SECTION -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="{{ config('system.app_name') }} is a Binary MLM program, aims to manage, and systematically distribute income to members">
    <meta name="keywords" content="admin, binary, networking, binary program, mlm program, networking program, flush out pairing, pairing">
    <meta name="author" content="Jomer Avengoza">

    @include('layouts.partial.header')

</head>

<body class="page-sound" data-base-url="">

<!--[if lt IE 9]>
<p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

@yield('content')

@include('layouts.partial.corePlugins')

@yield('pageIncludes')

@include('layouts.partial.googleAnalytics')

</body>
<!-- END BODY -->

</html>