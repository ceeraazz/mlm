<link href="{{ url('assets/fonts/open-sans.css') }}" rel="stylesheet">
<link href="{{ url('assets/fonts/oswald.css') }}" rel="stylesheet">

<link href="{{ url('assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
<!--/ END GLOBAL MANDATORY STYLES -->

<!-- START @PAGE LEVEL STYLES -->
<link href="{{ url('assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ url('assets/global/plugins/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
<link href="{{ url('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">
<link href="{{ url('assets/admin/css/reset.css') }}" rel="stylesheet">
<link href="{{ url('assets/admin/css/layout.css') }}" rel="stylesheet">
<link href="{{ url('assets/admin/css/components.css') }}" rel="stylesheet">
<link href="{{ url('assets/admin/css/plugins.css') }}" rel="stylesheet">
<link href="{{ url('assets/admin/css/themes/default.theme.css') }}" rel="stylesheet" id="theme">
<link href="{{ url('assets/admin/css/pages/sign.css') }}" rel="stylesheet">
<link href="{{ url('assets/admin/css/custom.css') }}" rel="stylesheet">

<link href="{{ url('custom/css/globalCustom.css') }}" rel="stylesheet">

<?php
    $page_title = str_replace('-', ' ', Request::segment(2));
?>

<title>{{ ($page_title != null) ? ucwords($page_title) . ' | ' : null }} {{ SystemSettings('app_name') }}</title>