@extends('layouts.loginLayout')
@section('content')
    <div class="sign-up" id="sign-wrapper">

        {{ BootstrapAlert() }}
        <!-- Login form -->
        {{ Form::open([
                'url'=>url(sprintf('auth/sign-up%s', $suffix)),
                'class'=>'sign-in form-horizontal shadow rounded no-overflow'
            ]) }}
        <div class="col-md-6 col-xs-12">

            <div class="sign-header">
                <div class="form-group">
                    <div class="sign-text">
                        <span>{{ Lang::get('labels.member_details') }}</span>
                    </div>
                </div><!-- /.form-group -->
            </div><!-- /.sign-header -->
            <div class="sign-body">
                @include('widgets.basic_profile_fields')
            </div><!-- /.sign-body -->

        </div>
        <div class="col-md-6 col-xs-12">
            <div class="sign-header">
                <div class="form-group">
                    <div class="sign-text">
                        <span>{{ Lang::get('labels.account_details') }}</span>
                    </div>
                </div>
            </div>
            <div class="sign-body">
                <div class="form-group">
                    {{ validationError($errors, 'have_activation') }}
                    <label class="control-label">{{ Lang::get('members.have_activation') }}</label>
                    {{ Form::select('have_activation', [
                        'yes'=>Lang::get('labels.have_activation'),
                        'no'=>Lang::get('labels.no_activation')
                    ], old('have_activation'), [
                        'class'=>'form-control have_activation chosen-select',
                    ]) }}
                </div>
                <div class="form-group {{ ($referral != null) ? 'hidden' : '' }}">
                    {{ validationError($errors, 'upline_id') }}
                    <label class="control-label">{{ Lang::get('labels.upline_id') }}</label>
                    {{ Form::text('upline_id', old('upline_id', $referral), [
                        'class'=>'form-control to-disable'
                    ]) }}
                </div>

                <div class="form-group {{ ($referral != null) ? 'hidden' : '' }}">
                    {{ validationError($errors, 'sponsor_id') }}
                    <label class="control-label">{{ Lang::get('labels.sponsor_id') }}</label>
                    {{ Form::text('sponsor_id', old('sponsor_id', $referral), [
                        'class'=>'form-control to-disable'
                    ]) }}
                </div>

                <div class="form-group">
                    {{ validationError($errors, 'account_id') }}
                    <label class="control-label">{{ Lang::get('labels.account_id') }}</label>
                    {{ Form::text('account_id', old('account_id'), [
                        'class'=>'form-control to-disable'
                    ]) }}
                </div>

                <div class="form-group">
                    {{ validationError($errors, 'activation_code') }}
                    <label class="control-label">{{ Lang::get('labels.activation_code') }}</label>
                    {{ Form::text('activation_code', old('activation_code'), [
                        'class'=>'form-control to-disable'
                    ]) }}
                </div>

                <div class="form-group">
                    {{ validationError($errors, 'node_placement') }}
                    <label class="control-label">{{ Lang::get('members.node_placement') }}</label>
                    {{ Form::select('node_placement', [
                        'left'=>Lang::get('labels.left'),
                        'right'=>Lang::get('labels.right')
                    ], old('node_placement'), [
                        'class'=>'form-control chosen-select to-disable',
                    ]) }}
                </div>
            </div>
            <div class="sign-footer">
                <div class="form-group">
                    <button type="submit" name="signup" class="btn btn-theme btn-lg btn-block no-margin rounded spinner">{{ Lang::get('labels.sign_up') }}</button>
                </div>
            </div>
            {{ Form::close() }}

            <!--/ Login form -->

        <div class="col-md-12 col-xs-12">
            <p class="text-muted text-center sign-link">{{ Lang::get('labels.have_account') }} <a href="{{ url('auth/login') }}"> {{ Lang::get('labels.login') }}</a></p>
        </div>

    </div>
@stop

@section('pageIncludes')
    <script type="text/javascript">
        $(function(){
            $('.have_activation').on('change', function(){
                var selected = $(this).children('option:selected').val();

                if (selected == 'no'){
                    $('.to-disable').attr('disabled', true);
                } else {
                    $('.to-disable').removeAttr('disabled');
                }
            }).trigger('change');
        })
    </script>
@stop